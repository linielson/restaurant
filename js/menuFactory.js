angular.module("restaurant").factory("menuFactory", function ($http) {
  var _getMenu = function () {
    return $http.get("http://localhost:3412/menu");
  };
  return {
    menu: _getMenu
  };
});
