"use strict";

var express = require("express");
var bodyParser = require("body-parser");
var app = express();

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.listen(process.env.PORT || 3412);

var createMenuItem = function(description, price) {
  return {
    description: description,
    price: price
  };
};

var menu = [
  createMenuItem("Hamburger", 3),
  createMenuItem("French fries", 5),
  createMenuItem("Soda", 2.5),
  createMenuItem("Beer", 3.5),
  createMenuItem("Water", 4),
  createMenuItem("Coca-Cola", 5),
  createMenuItem("Whisky", 12),
  createMenuItem("Juice", 20)
];

var createOrder = function(order) {
  return {
    orderItem: order,
    date: new Date(),
    subtotal: order.quantity * order.item.price
  };
};

var createOrders = function() {
  var _orders = [];

  var _getOrders = function() {
    return _orders;
  };

  var _getTotalOrder = function() {
    var totalOrder = 0;

    _orders.forEach(function(item) {
      totalOrder += item.subtotal;
    });

    return totalOrder;
  };

  var _addOrder = function(orderItem) {
    _orders.push(createOrder(angular.copy(orderItem)));
    delete $scope.order;
    $scope.orderForm.$setPristine();
  };

  var _removeOrder = function() {
    _orders = _orders.filter(function(orderItem) {
      if (!orderItem.selected) {
        return orderItem;
      }
    });
  };

  var _hasSelectedSomeOrder = function() {
    return _orders.some(function(orderItem) {
      return orderItem.selected;
    });
  };

  return {
    getOrders: _getOrders,
    addOrder: _addOrder,
    getTotalOrder: _getTotalOrder,
    removeOrder: _removeOrder,
    hasSelectedSomeOrder: _hasSelectedSomeOrder
  };
};

var orders = createOrders();

app.get("/menu", function(req, res) {
  console.log("Consultando o cardápio");
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  res.json(menu);
});

app.post("/orders", function(req, res) {
  console.log("Gravando o pedido");
  console.log(req.body);
  orders.push(req.body);
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  res.json(true);
});

app.all("*", function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

// app.get("/orders", function(req, res) {
// res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
//   res.header("Access-Control-Allow-Headers", "Content-Type");
// res.json(orders);
// });

// app.get("/item/1", function (req, res) {
//   res.json({description: "Agua", imagem: "http://www.paodeacucar.com.br/img/uploads/1/503/486503x200x200.jpg", detalhes: "Garrafa de 300ml", price: 4});
// });

// app.get("/item/2", function (req, res) {
//   res.json({description: "Coca-Cola", imagem: "http://www.paodeacucar.com.br/img/uploads/1/806/484806x200x200.jpg", detalhes: "Lata de 350ml", price: 5});
// });

// app.get("/item/3", function (req, res) {
//   res.json({description: "Whisky", imagem: "http://www.paodeacucar.com.br/img/uploads/1/111/477111x200x200.jpg", detalhes: "Dose", price: 12});
// });
