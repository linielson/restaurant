angular.module("restaurant").controller("restaurantCtrl", function($scope, $http, menuFactory) {
  "use strict";

  menuFactory.menu().success(function(data) {
    $scope.menuItems = data;
  });

  $scope.menuTitle = "Cardápio";
  $scope.orderTitle = "Pedido";

  $scope.orderMenuBy = function(column) {
    $scope.ordinationMenu = column;
    $scope.directionOrder = !$scope.directionOrder;
  };
});
