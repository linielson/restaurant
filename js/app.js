angular.module("restaurant", ["ngRoute"]);
angular.module("restaurant").config(function($routeProvider) {
  $routeProvider.when("/restaurant", {
    templateUrl: "view/orders.html",
    controller: "restaurantCtrl"
  });
  $routeProvider.otherwise({redirectTo: "/restaurant"});
});
